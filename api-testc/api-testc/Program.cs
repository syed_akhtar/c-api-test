﻿using System;
using System.IO;
using System.Net;

//install NuGet Package Newtonsoft.Json before using this
using Newtonsoft.Json.Linq;

namespace api_testc
{
   
    class Program
    {
        //file name and API call saved in strings
        private const string fileName = "messages.txt";
        private const string url = "https://api.bitbucket.org/2.0/repositories/calanceus/api-test/commits";

        static void Main(string[] args)
        {

            string result = string.Empty;
            string message = string.Empty;
            
            //try connecting to the API and save the result
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                using (Stream stream = response.GetResponseStream())
                using (StreamReader reader = new StreamReader(stream))
                result = reader.ReadToEnd();
                Console.WriteLine("API response recived");
            }
            
            //if connection fails
            catch(Exception e)
            {
                Console.WriteLine(e);

                //only to stop the console from disappearing press return to close the console
                Console.ReadLine();
            }
            finally
            {
                //accessing the messages from the whole response 
                JObject joResponse = JObject.Parse(result);
                JArray rjArray = (JArray)joResponse["values"];
                for (int i = 0; i < rjArray.Count; i++)
                {
                    //saving messages from commits in cronological order
                    message = rjArray[i]["message"].Value<string>() + "\r\n" + message;
                }

                //writing all the messages to file
                File.WriteAllText(fileName, message);
               
                Console.WriteLine("commits saved in file messages.txt relative path \\api-testc\\bin\\Debug\\netcoreapp2.1\\messages.txt ");

                //only to stop the console from disappearing press return to close the console
                Console.ReadLine();
            }
            

        }

       
    }
}
